var login_page = function() {
    this.clickCreateAccountLink = function() {
        element(by.xpath('/html/body/section/div/div/div[1]/div[2]/div/p/a')).click();
    }

    this.enterEmail = function(value) {
        element(by.name('email')).sendKeys(value);
    }

    this.enterPassword = function(value) {
        element(by.name('password')).sendKeys(value)
    }

    this.forgetPasswordLink = function() {
        element(by.xpath('/html/body/section/div/div/div[1]/div[2]/div/form/a'))
    }

    this.clickLoginButton = function() {
        element(by.id('submit_button')).click();
    }

    this.clickToggle = function() {
        element(by.xpath('//*[@id="form-toggler"]')).click()
    }

    this.enterMobileNumber = function(value) {
        element(by.id('phone_number')).sendKeys(value)
    }

    this.websiteLogo = function() {
        element(by.xpath('/html/body/section/div/div/div[2]/div/a/img'))
    } 

}
module.exports = new login_page