var Signup_page = function()  {
    this.enterEmail =(email)=> {
        element(by.name('email')).sendKeys(email)
    }

    this.enterPassword = (password) => {
        element(by.name('password')).sendKeys(password)
    }

    this.enterConfirmPassword = (confirmPassword) => {
        element(by.name('confirm_password')).sendKeys(confirmPassword)
    }

    this.formSubmit =() => {
        element(by.name('confirm_password')).submit()
    }

}
module.exports = new Signup_page