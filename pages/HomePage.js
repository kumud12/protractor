var homepagelocator = require('../locators/HomePageLocators');
var home_page = function() {
    this.enterGetStartedEmail = function(value) {
        element(by.name('email')).sendKeys(value);
    }

    this.clickGetStartedEmailButton = function() {
        element(by.id('submit_button')).click();
    }

    this.clickAboutUsLink = function() {
        element(by.xpath('/html/body/header/nav/div/div/ul/li[1]/a')).click();
    }

    this.clickContactUsLink = function() {
        element(by.xpath('/html/body/header/nav/div/div/ul/li[2]/a')).click();
    }

    this.clickLoginLink = function() {
        element(by.xpath('/html/body/header/nav/div/div/ul/li[3]/a')).click();
    }

    this.clickSignupLink = function() {
        element(by.xpath('/html/body/header/nav/div/div/ul/li[4]/a')).click();
    }

    this.clickHostCohostGetStartedButton = function() {
        element(by.xpath('/html/body/main/section/div/div/div[1]/div/div[2]/a')).click();

    }

    this.clickGuestGetStartedButton = function() {

        element(by.xpath('/html/body/main/section/div/div/div[2]/div/div[2]/a')).click();

    } 

    this.clickCleanerGetStartedButton = function() {

        element(by.xpath('/html/body/main/section/div/div/div[3]/div/div[2]/a')).click();

    }

    this.clickChatBotButton = function() {
        element(by.xpath('//*[@id="chatBot"]/div/div/div/a')).click();
    }

    this.getGetStartedEmailValue = function() {
       return  element(by.id('Email')).getAttribute('value');
    }
}
module.exports = new home_page