// import pages as pg from "../pages/HomePage";
var home_page = require('../pages/HomePage.js');
describe('We are in Trip TnT Home page', function () {

    // Get Trip TnT homepage before Each Test
    beforeEach(function () {
        browser.waitForAngularEnabled(false);
        browser.get('http://40.122.109.2/')
    })

    //  Test if Get Started link functions
    it('should go to sign up page with email', function () {
        home_page.enterGetStartedEmail("kharel.kumud02@gmail.com");
        home_page.clickGetStartedEmailButton();
        expect(home_page.getGetStartedEmailValue()).toEqual('kharel.kumud02@gmail.com');

    })

    // Test if the Host/CoHost Get Started Button Works
    it('should click the Host/CoHost Get Started Button', function () {
        home_page.clickHostCohostGetStartedButton();
        expect(browser.getTitle()).toEqual('Signup | Host');
    })

    //  Test if the Guest Get Started Button works
    it('should click guest get started link', function () {
        home_page.clickGuestGetStartedButton();
        expect(browser.getTitle()).toEqual('Signup | Guest');

    })


    // Test if the Cleaner Get Started Button works 
    it('should click cleaner get started link', function () {
        home_page.clickGuestGetStartedButton();
        expect(browser.getTitle()).toEqual('Signup | Guest');

    })


    // Test if the Login Link works
    it('should click the Login link ', function () {
        home_page.clickLoginLink();
        expect(browser.getTitle()).toEqual("Login | TripTnT");
    })

    // Test if the signup link works
    it('should click the signup link', function () {
        home_page.clickSignupLink();
        expect(browser.getTitle()).toEqual("Signup| TripTnT");
    })

    // Test if the contact us link works
    it('should click the contact us link', function () {
        home_page.clickContactUsLink();
        expect(browser.getTitle()).toEqual('TripTnT');
    })

    // Test if about us link works
    it('should click the about us link', function () {
        home_page.clickAboutUsLink();
        expect(browser.getTitle()).toEqual("TripTnT")
    })

    // Test if chatbot button works
    it('should open the chatbot window', function() {
        home_page.clickChatBotButton();
        expect(browser.getTitle()).toEqual("TripTnT");
    })
})