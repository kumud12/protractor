// import pages as pg from "../pages/LoginPage";
var login_page = require('../pages/LoginPage.js');

describe('We are in Trip TnT login page', function () {

    // before Each 
    beforeEach(function () {
        browser.waitForAngularEnabled(false);
        browser.get('http://40.122.109.2/login/')
    })


    // test for valid email and valid password
    xit('it should login for valid email and valid password', function () {
        login_page.enterEmail("kharel.kumud02@gmail.com")
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual("lodfdsl")

    })


    // test for valid email and invalid password
    xit('it should show validation for valid email and invalid password', function () {
        login_page.enterEmail("kharel.kumud02@gmail.com")
        login_page.enterPassword('sdssssadsaf')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual("lodfdsl")

    })


    //test for invalid email and invalid password
    xit('it should validation message for invalid email and invalid password', function () {
        login_page.enterEmail("kharel.kumusdsadsadd02@gmail.com")
        login_page.enterPassword('111sadsad111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual("lodfdsl")

    })


    // test for invalid email and empty password
    xit('it should login for valid email and valid password', function () {
        login_page.enterEmail("kharel.sdsdkumud02@gmail.com")
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual("lodfdsl")

    })




    // test for empty email and empty password
    xit('it should provide validation message to enter the email and password', function () {
        login_page.enterEmail('')
        login_page.enterPassword('')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    //test for empty email and invalid password
    xit('it should provide validation message for empty email and invalid password', function () {
        login_page.enterEmail('');
        login_page.enterPassword('sdsad');
        login_page.clickLoginButton();
        expect(browser.getTitle()).toEqual('Login | TripTnT');

    })

    // test for empty email and valid password
    xit('it should provide validation message for empty email and valid password', function () {
        login_page.enterEmail('')
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')

    })

    //test for valid email and empty password 
    xit('it should provide validation message for valid email and empty password', ()=>{
        login_page.enterEmail('host1@yopmail.com')
        login_page.enterPassword('')
        login_page.clickLoginButton() 
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    // test for invalid email and valid password

    xit('it should provide validation message for invalid email and valid password', () => {
        login_page.enterEmail('lol@lol.com')
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })


    // test for valid phone number and valid password
    xit('it should login for valid phone number and valid password', () => {
        login_page.clickToggle()
        login_page.enterMobileNumber('893849432')
        login_page.enterPassword('lol')
        login_page.clickLoginButton();
        expect(browser.getTitle()).toEqual("lol")
        
    })

    // test for valid phone number and invalid password
    it('it should show validation message for valid phone number and invalid password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('897979797978')
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })



    // test for invalid phone number and valid password
    it('should show validation message for invalid phone number and valid password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('897979797978')
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    // test for invalid phone number and invalid password
    it('should show validation message for invalid phone number and invalid password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('897979797978')
        login_page.enterPassword('111sdsd111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    // test for the empty phone number and valid password
    it('should show validation message for empty phone number and valid password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('')
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    // test for empty phone number and invalid password
    it('should show validation message for empty phone number and invalid password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('897979797978')
        login_page.enterPassword('111dfd111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    // test for valid phone number and empty password
    it('should show validation message for valid phone number and empty password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('897979797978')
        login_page.enterPassword('')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

    // test for invalid phone number and empty password
    it('should show validation message for invalid phone number and valid password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('897979797978')
        login_page.enterPassword('111111')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })

     // test for invalid phone number and empty password
     it('should show validation message for empty phone number and empty password', () =>{
        login_page.clickToggle()
        login_page.enterMobileNumber('')
        login_page.enterPassword('')
        login_page.clickLoginButton()
        expect(browser.getTitle()).toEqual('Login | TripTnT')
    })
})