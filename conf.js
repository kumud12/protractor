var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
exports.config = {
  framework: 'jasmine',
  // capabilities: {
  //   browserName: 'firefox'
  // },
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['testSuite/SignupPageTest.js'],
  onPrepare: () => {
    let globals = require('protractor/built');
    browser.ignoreSynchronization = true;
    browser.manage().window().maximize();
    jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
      savePath: "./test/reports",
      takeScreenshots: true,
      takeScreenshotsOnlyOnFailures: true,
      fixedScreenshotName: true,
      cleanDestination: false,
      showPassed: false,
      fileName: 'MyReportName',
      fileNameSeparator: '_'
    }))
  },
};