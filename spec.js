describe("Addition of the calculator", function() {
    it ("should add the two numbers", function() {
        browser.get("https://juliemr.github.io/protractor-demo/");
        browser.sleep(3000);

        element(by.model('first')).sendKeys("10");
        browser.sleep(3000);

        element(by.model("second")).sendKeys("20");
        browser.sleep(3000);

        element(by.id("gobutton")).click();
        browser.sleep(3000);

        var resultElement = element(by.xpath("/html/body/div/div/form/h2"));
        expect (resultElement.getText()).toEqual("30");
    })
})

describe("Substraction of the calculator", function() {
    it ("should substract two numbers", function() {
        browser.get("https://juliemr.github.io/protractor-demo/");
        browser.sleep(3000);

        element(by.model('first')).sendKeys("10");
        browser.sleep(3000);

        var select = element(by.model('operator'));
        select.$('//option[contains(text(),'-')]').click(); 
        element(by.model("second")).sendKeys("20");
        browser.sleep(3000);

        
        element(by.id("gobutton")).click();
        browser.sleep(3000);

        var resultElement = element(by.xpath("/html/body/div/div/form/h2"));
        expect (resultElement.getText()).toEqual("30");

    })
})


describe("test", function() {
    it ("should ", function() {
        browser.get("https://angularjs.org/");
        element(by.model("yourName")).sendKeys("lol");
        var textElement = element(by.xpath("/html/body/div[2]/div[1]/div[2]/div[2]/div/h1"));
        // expect(textElement.getText()).toEqual("hello");
        textElement.getText().then(response => console.log(response))

        // console.log(expect(textElement.getText()).toEqual("hello"));  
        // browser.sleep(300000)  
    })
})